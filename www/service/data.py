#! /usr/bin/python3

from sqlalchemy import Column, create_engine, String, Integer, TIMESTAMP, text
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
engine = create_engine("postgresql+psycopg2://postgres:1@localhost/logger", isolation_level="READ UNCOMMITTED")


class Type(Base):
    __tablename__ = 'myType'
    id = Column(Integer, primary_key=True)
    name = Column(String(32))
    alias = Column(String(16))

    def __repr__(self):
        return "<Type(id=%d, name=%s, alias=%s)>" % (self.id, self.name, self.alias)


class Data(Base):
    __tablename__ = 'myData'
    id = Column(Integer, primary_key=True)
    number = Column(Integer)
    ts = Column(TIMESTAMP)  # hmi时间
    created = Column(TIMESTAMP, server_default=text('NOW()'))  # 系统时间
    type = Column(String(32))
    factory = Column(Integer)


Base.metadata.create_all(engine)
