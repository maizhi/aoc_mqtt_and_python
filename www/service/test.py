from datetime import datetime

from sqlalchemy import create_engine, TIMESTAMP
from sqlalchemy.orm import sessionmaker

from data import Data
import time

t = (2009, 2, 17, 17, 3, 38, 1, 48, 0)
engine = create_engine("postgresql+psycopg2://postgres:1@localhost/logger", isolation_level="READ UNCOMMITTED")
Session = sessionmaker(bind=engine)
# dt = datetime.strptime('2018-03-23T06:19:48.929591', '%Y-%m-%dT%H:%M:%S.%f')
data = Data(number=100,
            ts=datetime.strptime('2018-03-23T06:19:48.929591', '%Y-%m-%dT%H:%M:%S.%f'), type='加工')
session = Session()
session.add(data)
session.commit()
